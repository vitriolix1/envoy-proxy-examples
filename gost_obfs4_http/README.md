# Envoy GOST obfs4 http proxy

This is an example of a proxy setup for use with a [fork](https://github.com/greatfire/apps-android-wikipedia-envoy) of the Wikipedia app with [Envoy](https://github.com/greatfire/envoy)

Other examples are [here](../README.md)

This example uses [GOST](https://v2.gost.run/en/) ([github](https://github.com/ginuerzh/)) to provide a standard HTTP proxy tunneled though Obfs4. 

GOST is not packaged for easy install on Debian, so we grab the binary from their github page. If you're using a platform that's not `amd64`, you'll need to change the URL to the correct binary for your platfrom, the URL for the ARMv8 version is commented out in there, find the others [here](https://github.com/ginuerzh/gost/releases).

GOST can do a lot more than this, it also supports Shadowsocks, Simple-obfs, and more, and can combine them. For example, you can tunnel Shadowsocks though Obfs4. This example is pretty simple, but could serve as the basis for using GOST for other protocols.

## Configuration

GOST is configured to listen on port `58243` by default. This can be customized in the `gost_obfs4_http.json` config file. Connections are limited to `*.wikipedia.org` and `*.wikimedia.org` domains, also in that file. Listen protocols are also configured there.

Like Obfs4proxy, we'll need to get it up and running before we're able to get the authorization info to connect (GOST actually embeds the obfs4proxy code). We'll cover getting that info in the Post Install section.

## Ansible

Add your target host to any group in your inventory file (`inventory.ini`):

```
[all]
192.168.64.3
```

If sudo on the target requires a password:

`ansible-playbook -i inventory.ini gost_obsf4_http.yaml -K`

if not, you can exclude the `-K` option.

## Post Install

Check the status of the server (run from the target machine using sudo or as root):

`service gost_obfs4_http status`

The info we need to connect to the server is in the logs there, if it started correctly. The pager can "truncate" the lines of that output, so running something like this might be necessary:

`journalctl -u gost_obfs4_http --no-pager`

You should see a line something like this:

```
Mar 15 14:27:41 envoy-x86 gost[1633]: 2022/03/15 14:27:41 obfs.go:625: [obfs4] server inited: +obfs4://0.0.0.0:58243/?cert=Sxe0yY32ng7Vr3nUM6TFxiRwTCZXLOSYtCrNUEFjSPTugaXAoR2tgNN1ciWHBlsRH5qpCQ&iat-mode=0
```

The info after the `?` is the auth info needed to connect.

You can test by running another copy of GOST. You can run on any host with access to the server, or the server itself. We'll need the correct IP address, but otherwise the logged URL is the one we want:

```
gost -D -L :8123 -F "obfs4://192.168.64.3:58243/?cert=Sxe0yY32ng7Vr3nUM6TFxiRwTCZXLOSYtCrNUEFjSPTugaXAoR2tgNN1ciWHBlsRH5qpCQ&iat-mode=0"
```

This listens on port `8123` and the `-D` flag enables debug logging. Make a request to the helper GOST like this:

```
curl -x socks5h://localhost:8123/ https://www.wikipedia.org/
```

Test access control by requesting another host:

```
curl -x socks5h://localhost:8123/ https://www.google.com/
```

and it should fail. Note also that using `socks5://` instead of `socks5h://` with curl will look up the host locally, and make the request by IP, which GOST will reject, because the IP doesn't match the whitelisted hostnames. That's the expected use case with Envoy based clients, so it should only be an issue in testing.

TODO: what EnvoyURL format is needed?
