
# Envoy Wikipedia HTTP proxy

This is an example of a proxy setup for use with a [fork](https://github.com/greatfire/apps-android-wikipedia-envoy) of the Wikipedia app with [Envoy](https://github.com/greatfire/envoy)

Other examples are [here](../README.md)

This example uses [nginx](https://nginx.org/) configured with a small block of Lua code to restrict access to our target domains, so we're not running an open proxy. Access control to the proxy can be done in a number of ways, but for this simple example, we'll restrict the user to `.wikipedia.org` and `.wikimedia.org` domains.


You'll need valid SSL certs, or at least ones trusted by your devices, to use HTTPS to connect to the proxy. For a test setup, you can use self signed certs ([mkcert](https://github.com/FiloSottile/mkcert) is a useful helper for that) or request certificates from [Let's Encrypt](https://letsencrypt.org/). Configuring certificates is outside the scope of this example, but know that you will need valid certs to use HTTPS.

By default, we use a hostname of `example-wikipedia-proxy`. If you want to use something differnt, update the `proxy_hostname` value in `envoy_proxy.yaml`. If you do, you'll need to name your cert files to match as well. Get or generate your certs, and place them in the `certs/` subdirectory. For the example hostname, they should be named `example-wikipedia-proxy.pem` and `example-wikipedia-proxy-key.pem`.

If you have `mkcert` installed you can:
```
cd certs
mkcert example-wikipedia-proxy
cd ..
```
To get some self signed certs in place.


## Configureation

The file `envoy_wikipedia_proxy.conf.j2` can be edited before deploy if you want to customize the nginx config.

## Ansible

Add your target host to the `envoy_proxies` group in your inventroy (`inventory.ini` in this example):

```
[envoy_proxies]
192.168.64.2
```

If sudo on the target requires a password:

`ansible-playbook -i inventory.ini envoy_proxy.yaml -K`

if not, you can exclude the `-K` option.

## Post install

(if you customized the host name, use that in place of `example-wikipedia-proxy` for the examples below)

The proxy should be listening for HTTP connections on port 80 and HTTPS on port 443. You can test it with curl:

`curl -H 'Url-Orig: https://en.wikipedia.org/wiki/Main_Page' -H 'Host-Orig: en.wikipedia.org' http://example-wikipedia-proxy/wikipedia/`
`curl -H 'Url-Orig: https://en.wikipedia.org/wiki/Main_Page' -H 'Host-Orig: en.wikipedia.org' https://example-wikipedia-proxy/wikipedia/`

If using self-signed certs (note: this will not work with Envoy! but useful for testing) curl needs a `--insecure` flag:

`curl --insecure -H 'Url-Orig: https://en.wikipedia.org/wiki/Main_Page' -H 'Host-Orig: en.wikipedia.org' https://example-wikipedia-proxy/wikipedia/`

Assuming everything worked, Envoy can be given a URL like `http://example-wikipedia-proxy/wikipedia/` or `https://example-wikipedia-proxy/wikipedia/`

