# Envoy obfs4proxy + SOCKS proxy

This is an example of a proxy setup for use with a [fork](https://github.com/greatfire/apps-android-wikipedia-envoy) of the Wikipedia app with [Envoy](https://github.com/greatfire/envoy)

Other examples are [here](../README.md)

This example uses the [Dante SOCKS server](https://www.inet.no/dante/) tunneled though [obfs4proxy](https://gitlab.com/yawning/obfs4) as packaged by Debian.

## Configuation

Dante is configured to not require a password, see [the documentation](https://www.inet.no/dante/doc/) if you want require logins. By default it listens on port `9595` only on localhost.

Obfs4proxy listens on port `58242` by default. Customize that by changing the `TOR_PT_SERVER_BINDADDR` value in `obfs4proxy.env`.
The `TOR_PT_ORPORT` should point to the service you are tunnling, change this to tunnel something other than Dante.

Obfs4proxy will write out the auth info you need to connenct in it's `pt_state` directory, by default, that's in `/usr/local/envoy/pt_state`. See below for how to find and use that.

## Ansible

Add your target host to the `obfs4proxy_socks_servers` group in your inventroy (`inventory.ini` in this example):

```
[obfs4proxy_socks_servers]
192.168.64.2
```

If sudo on the target requires a password:

`ansible-playbook -i inventory.ini obfs4proxy_socks_server.yaml -K`

if not, you can exclude the `-K` option.

## Post instatll

Check the status of the servers (run from the target machine using sudo or as root):

`service obfs4proxy status`
`service danted status`

To get the auth info to connect, ssh in to the target host, and look at `/usr/local/envoy/pt_state/obfs4_bridgeline.txt`, you should see a line like:

```
Bridge obfs4 <IP ADDRESS>:<PORT> <FINGERPRINT> cert=1cTu3WucYjDkZe3LAqvKsW4Cq3ZbYZviOqPkExl0weg7jLd1K8GBIk3qPfLZUcom7KgSVA iat-mode=0
```

The username used to connect will be the `cert` and `iat-mode` parameters combinded with a semicolon `;`, in this example: `cert=1cTu3WucYjDkZe3LAqvKsW4Cq3ZbYZviOqPkExl0weg7jLd1K8GBIk3qPfLZUcom7KgSVA;iat-mode=0`. Some clients support base64 encoding this value, which may be needed, since it can contain characters like `/` that throw off parsing.

We can test this using [GOST](https://v2.gost.run/en/) as a helper. It supports passing auth params as a GET param. Start up the helper using the values from your obfs4proxy server:

```
gost -D -L :8123 -F "socks+obfs4://192.168.64.2:58242/?cert=1cTu3WucYjDkZe3LAqvKsW4Cq3ZbYZviOqPkExl0weg7jLd1K8GBIk3qPfLZUcom7KgSVA;iat-mode=0"
```

Then you can use curl via the helper GOST proxy:

```
curl -x socks5h://localhost:8123/ https://www.wikipedia.org/
```

TODO: what EnvoyURL format is needed?


