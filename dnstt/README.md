# Envoy DNSTT

This is an example setup for [DNSTT](https://www.bamsoftware.com/software/dnstt/), an encrpyted tunnel over DNS. Tunneling over DNS isn't fast or efficient, so it's best used to transfer config data for faster proxies for use with [Envoy](https://github.com/greatfire/envoy)

Other examples are [here](../README.md)

The DNSTT source code is availbe [here](https://github.com/Mygod/dnstt)

In this example, we set up a DNSTT server that proxies a simple nginx config to serve configuation information to clients.

DNSTT is not very fast or efficient, but it is ideally hard to block. It's not recomended for transferring large amounts of data. Envoy uses DNSTT to request a small JSON file with configuration data.

## Configuration

You'll need a domain name you can add records to, and basic familarity with DNS configuration. We're going to set up a subdmain for DNSTT, so it will not affect any other uses of that domain name. We'll use `example.org` for this example. Our tunnel domain will be `t.example.org` and we'll need to add a nameserver record as well. This example uses `tns.example.org`. You can use any valid hostname you like, though shorter is better for the tunnel host. In this example, we'll use 192.168.64.2 as the DNSTT server address. 

Update the `tunnel_domain` variable in the ansible playbook to your chosen domain name.

You'll need to add A and AAAA records for your DNSTT server's IP4 and IP6 addresses, this is `tns.example.org` in this example.

Then you'll need to create an NS record that sets `tns.example.org` as the namesever for `t.example.org`

DNS setup is also explained [here](https://www.bamsoftware.com/software/dnstt/)

For example, if we have a server with IP4: 192.168.64.2 and IP6 2001:db8::2, we'd want to set up:

```
A        tns.example.org  points to 192.168.64.2
AAAA     tns.example.org  points to 2001:db8::2
NS       t.example.org    is managed by tns.example.org
```

## Ansible

Add your target host to the `dnstt_servers` group in your inventory (`inventory.ini` in this example):

```
[dnstt_servers]
192.168.64.2
```

If sudo on the target requires a password:

`ansible-playbook -i inventory.ini dnstt.yaml -K`

if not, you can exclude the `-K` option.

## Post install

Check the status of the servers (run from the target machine using sudo or as root):

* `service dnstt status`
* `service nginx status`

For client connections, you'll need the server's public key. It's located in `/usr/local/envoy/dnstt/server.pub`. Copy this file to your client machine. 

Get and build the client source:
```
apt install glang # if needed or download from https://go.dev/dl/
git clone https://www.bamsoftware.com/git/dnstt.git
cd dnstt/dnstt-client
go build
```

First, we can test with a direct connection. This direct connection doesn't provide any privacy or protection, we just want to verify the server works.

Start the client with a direct connection on port 7000:
`./dnstt-client -udp 192.168.64.2:53 -pubkey-file server.pub 127.0.0.1:7000`

Now make an http request though the tunnel:
`curl http://127.0.0.1:7000/config.json`


If everything is working as expected, we can move on to testing a private connection
with DNS servers. Select a DNS resolver to use, DNS over HTTP (DoH) or DNS over TLS (DoT):
DoH:
* [https://github.com/curl/curl/wiki/DNS-over-HTTPS#publicly-available-servers](https://github.com/curl/curl/wiki/DNS-over-HTTPS#publicly-available-servers)

DoT:
* [https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Public+Resolvers#DNSPrivacyPublicResolvers-DNS-over-TLS%28DoT%29](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Public+Resolvers#DNSPrivacyPublicResolvers-DNS-over-TLS%28DoT%29)
* [https://dnsencryption.info/imc19-doe.html](https://dnsencryption.info/imc19-doe.html)

Start the client listening on port 7000:

DoH:

* `./dnstt-client -doh https://doh.example.com/dns-query -pubkey-file server.pub t.example.org 127.0.0.1:7000`

DoT:

* `./dnstt-client -dot 1.1.1.1:853 -pubkey-file server.pub t.example.org 127.0.0.1:7000`

Fetch the config file using curl (or any web browser):
`curl http://127.0.0.1:7000/wikiunblocked/config.json`

Check the client output for errors. A few errors are normal, for example DoH servers will somtimes return a 502, causing the client to sleep. Try different DNS resolvers if you have trouble.
