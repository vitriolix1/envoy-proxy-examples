# Envoy Wikipedia Shadowsocks proxy

This is an example of a proxy setup for use with a [fork](https://github.com/greatfire/apps-android-wikipedia-envoy) of the Wikipedia app with [Envoy](https://github.com/greatfire/envoy)

Other examples are [here](../README.md)

This example uses [shadowsocks-libev](https://github.com/shadowsocks/shadowsocks-libev) as packaged by Debian.

## Configuration

`shadowsocks-config.json` contains the settings for what cypher to use, and the password. Since the default password is published in this repo, you might want to change it.

The default cypher is `chacha20-ietf-poly1305`, this is the default used by shadowsocks-libev, but you may wish to change it as well.

The listen port (default 8388), can also be set in this file.

`shadowsocks-sysctl.conf` configures the system networking as recommended in the shadowsocks-libev docs

The shadowsocks-libev server does not support restricting access to certain URLs, but it is password protected.

## Ansible

Add your target host to the `shadowsocks_servers` group in your inventroy (`inventory.ini` in this example):

```
[shadowsocks_servers]
192.168.64.2
```

If sudo on the target requires a password:

`ansible-playbook -i inventory.ini envoy_proxy.yaml -K`

if not, you can exclude the `-K` option.

## Post install

To construct the envoy URL, we base64 endcode the standared `username:passowrd` parameter. Our example uses `chacha20-ietf-poly1305:aew4YahF2ox6yohj`

A simple way to do that from the command line:

```
> echo -n "chacha20-ietf-poly1305:aew4YahF2ox6yohj" | base64
Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTphZXc0WWFoRjJveDZ5b2hq
```

If our shadowsocks server is running on 192.168.64.2 on port 8388, we would use the url: `ss://Y2hhY2hhMjAtaWV0Zi1wb2x5MTMwNTphZXc0WWFoRjJveDZ5b2hq@192.168.64.2:8388/`
